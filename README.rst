.. header::

    ###Title### - page ###Page###/###Total###

.. footer::

    Muñoz & Peters - Thu 02 Jun 2016 04:15:08 PM CEST

==========================
cellcity
==========================

:Author: Esteban Muñoz & Irene Peters
:Version: 1.0
:Date: Thu 02 Jun 2016 04:15:08 PM CEST
:Public: `gitlab/emunozh/cellcity`_

.. _`gitlab/emunozh/cellcity`: https://gitlab.com/emunozh/cellcity/tree/master

.. contents:: Table of Contents
   :depth: 2

.. sidebar:: ODD Protocol

   The following documents follows the ODD protocol [Grimm.2010]_
   for the documentation of Agent Based Models (AMB)

------------------------------------------------------------------------------

.. raw:: pdf

    PageBreak oneColumn

Purpose
=======

The purpose of this model is to present a
minimal working example of an agent based model for the simulation of urban
systems with an underlying heat supply grid.

The aim of this model is to show a conceptional example of an agent-based model
for the simulation of the interaction between a building stock model and a
heat supply grid.
The building stock model simulates the aging of buildings (expressed as the
time since the last renovation) and the growth or decay of an infrastructure
grid depending on global and local variables.

The simulation Framework is a set of arrays describing the agents attributes
and the computed state transition probabilities of these agents.
The example is written in pure python. We make use of two well established
python libraries: (1) numpy for the representation of arrays and matrix
algebra computations and (2) matplotlib for the visualization of simulation
stages and results.

For this minimal working example we use a randomly generated world.
The distribution of buildings and the initial decision to connect them to the
heat supply grid as well as the initial renovation rate are normally
distributed.
On a second implementation of this model we aim to use these distributions to
generate a more realistic world.
We also aim to extend this model in order for it to take real world data as
input for the generation of synthetic cities.

On this minimal implementation we do not take into account socio-demographic
characteristics of building occupants and building owners.
We see the use of agents constructed upon socio-demographic parameters as an
extension of the model section estimating retrofit transition probabilities
and therefore not part of a minimal working example of the developed model.

Entities, State Variables and Scales
====================================

The model is implemented on a simple cell-grid with a random initial state.
The model computes states transition probabilities for each agent at each
simulation step.
The initial state of buildings is described as the age of buildings and the
number of retrofits performed on the building.
The simulation runs until each buildings has been retrofitted at least one time.
The states traditions are defined through a predefined threshold level for each
transition array defined as input variables.
The values for the transition array are computed as function of: (1) agents
parameters, (2) neighbourhood parameters and (3) global parameters.

The neighborhood of agents is defined as the moor neighborhood with a variable
neighborhood size, define by variable `k`.
The size of the neighborhood varies depends on the computed variable.

We define two type of agents: (1) buildings; and (2) grid sections.
We define an array for each parameter of the agent.
For the model implementation we define classes describing the agent space.
Each python class contains the different arrays with the agent variables, the
functions needed for the computation of transitions and the functions needed
for the generation of synthetic data.
Both agents interact for the computation of transitions array.

.. figure:: FIGURES/World.png
   :scale: 170%
   :alt: model UML

   **Figure 1: UML of the minimal model implementation**

The **Figure 1** shows the different classes defined for the simulation.
The two main agents defined in the framework are represented on classes:
(1) `BuildingSpace` and (2) `GridSpace`.
The class controlling the simulation inherits from both classes.
There is a forth class called `G`.
This class contains all the global variables of the model.

Building agent
--------------

The Building agents are described by two arrays.
`age` is the array containing the *age* of each building, a value of -1
represents an empty cell.
The initial age values are uniformly distributed between 0 (new construction)
and the global variable `maxInitBuildingYear`.
The `retrofits` array is the array containing the number of performed retrofits
on the buildings.
The initial number of retrofits per building has a maximum number of 1.
The initial number of building with a retrofit is predefine as a share of total
buildings, defined on global parameter `initalRenovationShare`.
The share of selected buildings to attribute them a performed retrofit are
selected based on a probability vector.
This vector is linearly align to the construction year.
The older the building the higher the probability of this building to have been
retrofitted in the past.

.. table:: Table 1: Buildings parameters and functions

    +-------------------------------+---------------------------+------------------------------------------------------------+
    | **Variables/Functions**       | **Range/Input**           | **Description:**                                           |
    +===============================+===========================+============================================================+
    | Buildings                     | [-1,                      | Years since last retrofit                                  |
    |                               | `maxInitBuildingYear`\+t] |                                                            |
    +-------------------------------+---------------------------+------------------------------------------------------------+
    | Renovations                   | [0, Inf]                  | Number of performed                                        |
    |                               |                           | retrofits on the building                                  |
    +-------------------------------+---------------------------+------------------------------------------------------------+
    | Retrofit                      | [0, 1]                    | Transition array for                                       |
    |                               |                           | retrofiting a building                                     |
    +-------------------------------+---------------------------+------------------------------------------------------------+
    | ConnectionWish                | [0, 1]                    | Transition array describing the desire of                  |
    |                               |                           | a Building to get connected to the heat supply grid,       |
    |                               |                           | see `connect()` below.                                     |
    +-------------------------------+---------------------------+------------------------------------------------------------+
    | generateWorld()               |                           | Generates random buildings on space                        |
    +-------------------------------+---------------------------+------------------------------------------------------------+
    | readWorld()                   | File path                 | Read a predefine world file                                |
    |                               |                           | describing buildings on space                              |
    +-------------------------------+---------------------------+------------------------------------------------------------+
    | initRenovations()             | `G.initialRenovatedShare` | Select a random set of buildings as renovated,             |
    |                               |                           | the total number of selected building is                   |
    |                               |                           | computed as share of total buildings with                  |
    |                               |                           | with global parameter `initialRenovatedShare`              |
    +-------------------------------+---------------------------+------------------------------------------------------------+
    | computeRetrofitsTransitions() | `amortization` and        | Compute the retrofit transition array as                   |
    |                               | `getRetrofitNeighbours()` | function of: (1) amortization time in year                 |
    |                               |                           | irequired to pay the retrofit investment;  and             |
    |                               |                           | (2) the number of neighbours that have                     |
    |                               |                           | performed a retrofit, see `getRetrofitNeighbours()`        |
    +-------------------------------+---------------------------+------------------------------------------------------------+
    | getRetrofitNeighbours()       | `G.neighbourhood` and     | Computes the number of neighbours (as in moor neighborhood |
    |                               | `G.retrofitsTimeSpan`     | k=`G.neighbourhood`) that have performed a retrofit        |
    |                               |                           | within `G.retrofitsTimeSpan` years.                        |
    +-------------------------------+---------------------------+------------------------------------------------------------+
    | connect()                     | `G.connectionThreshold`   | The descition to get or not connected into the grid is     |
    |                               |                           | randomly generated. The generated random number has to be  |
    |                               |                           | smaller that the global threshols `G.connectionThreshold`. |
    +-------------------------------+---------------------------+------------------------------------------------------------+


**Table 1** describes the defined building space.
Part of this definition are the functions describing the agents behaviours.

Grid section agent
------------------

The grid class defines a single value array `Grid` and a single transition
array `Grow`.
The value array contains the values representing the grid layout and a
boolean variable describing a connection to the building on the cell. Values of
-1 represent the absence of a grid section at that cell.

.. table:: Table 2: Grid parameters and functions

   +-----------------------------------+-----------------------------------+------------------------------------------------+
   | **Variables/Functions**           | **Range/Input**                   | **Description:**                               |
   +===================================+===================================+================================================+
   | Grid                              | [-1, 1]                           | Is a building connected to this grid section.  |
   +-----------------------------------+-----------------------------------+------------------------------------------------+
   | Grow                              | [0, 1]                            | Turansition array defining the gwoth behaviour |
   |                                   |                                   | of the heat supply grid.                       |
   +-----------------------------------+-----------------------------------+------------------------------------------------+
   | constructGrid()                   | Buildings                         | Constructs a synthetic grid based on           |
   |                                   |                                   | the buildings layout.                          |
   +-----------------------------------+-----------------------------------+------------------------------------------------+
   | readGrid()                        | File path                         | Reads a grid layout from a file.               |
   +-----------------------------------+-----------------------------------+------------------------------------------------+
   | computeGridTransitions()          | getBuildingsConnectionPotential() | Computes the transition array `Grow` based on  |
   |                                   |                                   | the number of buildings willing to connect to  |
   |                                   |                                   | the grith within a predefine catchment area.   |
   +-----------------------------------+-----------------------------------+------------------------------------------------+
   | getBuildingsConnectionPotential() | B.ConnectionWish and              | Computes the potential expansion for a grid    |
   |                                   | G.gridCatchmentArea               | cell.                                          |
   +-----------------------------------+-----------------------------------+------------------------------------------------+
   | makeGridGrow()                    |                                   | Expand the grid.                               |
   +-----------------------------------+-----------------------------------+------------------------------------------------+
   | closestPoint()                    | initial pointa and vector         | Returns the clossest point from a vector of    |
   |                                   | of points.                        | points.                                        |
   +-----------------------------------+-----------------------------------+------------------------------------------------+
   | drawPath()                        | initial and final point.          | Draws a path between two points.               |
   +-----------------------------------+-----------------------------------+------------------------------------------------+

Global parametets
-----------------

All global variables define in the model are store on a python class (not
represented on the UML diagram).
Global variables represent the scenario parameters.
For this small example we only simulate the "default" scenario. 
The variation of this global variables will impact the simulation result.
A systematic sensitivity analysis has not been performed on this variables.

**Table 3** lists the model global parameters, their default value and a small
description of the parameter.

.. table:: Table 3: Global parameters used in the simulation

    +---------------------+-------------+-------------------------------------------------------------+
    | **Parameter**       | **Default** | **Description**                                             |
    +=====================+=============+=============================================================+
    | connectionThreshold | 0.5         | Threshold for buildings to decide to get                    |
    |                     |             | connected to the heat supply grid.                          |
    +---------------------+-------------+-------------------------------------------------------------+
    | initialEnergyPrice  | 0           | Initial energy price, interpretation depends of             |
    |                     |             | energy index computation function.                          |
    +---------------------+-------------+-------------------------------------------------------------+
    | getEnergyPrice      | energyLog   | Function used for the computation of the energy price index |
    +---------------------+-------------+-------------------------------------------------------------+
    | B0                  | 10          | Transition probability B0 coefficient.                      |
    +---------------------+-------------+-------------------------------------------------------------+
    | retrofitsTimeSpan   | 2           | Time span to look for retrofits.                            |
    +---------------------+-------------+-------------------------------------------------------------+
    | neighbourhood       | 3           | k moor neighbourhood parameter.                             |
    +---------------------+-------------+-------------------------------------------------------------+
    | gridCatchmentArea   | 5           | Catchment area fro grid to expand.                          |
    +---------------------+-------------+-------------------------------------------------------------+
    | seed                | 5           | Random seed. needed for reproducibility of results.         |
    +---------------------+-------------+-------------------------------------------------------------+

Most of the global parameters are static values.
The parameter `getEnergyPrice` defines a function name rather than a static
value.
This parameter defines the function to be used for the estimation of the energy
price index within the model framework. 

**Figure 2** plots two different functions for the computation of energy price
index.
Both functions are completely fictional. 
These two function represent two different scenarios for the evolution of
energy prices and tell two different stories. 

The first function *[Ie = 1 + log(t+1)]* is the default function used in the
model.
This is a simple logarithmic function, it assumes a logarithmic increase of
energy prices.
The second function *[Ie = 1 + 0.4t - 0.008t^2]* is a polynomial function that
simulates an energy price peak at t=25.
Both functions are developed as function of time.

.. figure:: FIGURES/energy.png
   :scale: 60 %
   :alt: energy index

   **Figure 2: Scenarios for the possible energy price index development**

Global parameters listed on **table 4** describe the define parameters used for
the creation of the initial synthetic world.
This parameters are only needed if the initial world is a synthetic one.
The presented model implements only the use of a synthetic world for
simulation. 
The preparation of real data for the use on this type of simulation does not
form part of the minimal example presented here.

.. table:: Table 4: Input parameters for the creation of a synthetic world

    +-------------------------+-------------+-----------------------------------+
    | **Parameter**           | **Default** | **Description**                   |
    +=========================+=============+===================================+
    | connectionShare         | 0.1         | Share of buildings connected to   |
    |                         |             | the grid.                         |
    +-------------------------+-------------+-----------------------------------+
    | buildingsDensity        | 0.1         | Building density of the synthetic |
    |                         |             | world.                            |
    +-------------------------+-------------+-----------------------------------+
    | maxInitConstructionYear | 50          | Maximum value for building        |
    |                         |             | year                              |
    +-------------------------+-------------+-----------------------------------+
    | initialRenovatedShare   | 0.1         | Share of retrofitted buildings    |
    +-------------------------+-------------+-----------------------------------+
    | dim                     | [40, 40]    | Length and width of random world  |
    +-------------------------+-------------+-----------------------------------+


Process Overview ans Scheduling
===============================

The transition arrays are expressed as transition probabilities from state
**a** to state **b** giving a set of agent-own variables,
neighbour-look-up-variables and global variables.

The transition probability is expressed as a dichotomous variable (1=change and
0=no-change) while the input variables are continuous variables.
In order to compute this transitions we implement a logit function.
The defined functions used in this model are completely fictional, nonetheless
the model structure allows for this function to be calibrated with empirical
observations using a logistic regression model for the definition of the
corresponding logit functions.

  "If Y represents a probability that must be confined between 0 and 1, it
  seems reasonable to represent the function equation between X and Y in such
  a way that whenever Y is approaching either its upper or lower limit, it
  becomes increasingly difficult to produce a change of a given magnitude in
  Y . . . Rather than take changes in Y as proportional to a constant (β)
  times changes in X, this suggests that these changes in X need to be
  multiplied by a factor that becomes deflated as Y approaches either 0 or
  1." [Blalock.1979]_

The logit function can be formally expressed with the following formula:

.. math::

    Y = \frac{1}{1 + e^{-\beta_0 - \beta_1 X}}

**Figure 3** plots the normalized logit function used in the model for the
computation of array transitions. 
Below the figure we find the python code used for the definition of:

(a) The inverse logit function `inv_logit`.
(b) The `Retrofit` transition array as function of:
    (1) `retrofitNeighbours` and
    (2) `amortization`
(c) The `Grow` transition array as function of:
    (1) `connectBuildings`

.. figure:: FIGURES/logit.png
   :scale: 60 %
   :alt: logit

   **Figure 3: Example of a logit curve**

.. code-block:: python
   :linenos:
   
   def inv_logit(p): return np.exp(p) / (1 + np.exp(p))

.. code-block:: python
   :linenos:

   B2 = G.B0/np.max(retrofitNeighbours)
   B1 = np.log(G.B0/np.max(amortization * -1))
   p = -G.B0 + B1 * amortization * -1 + B2*retrofitNeighbours
   self.Retrofit = np.round(inv_logit(p))

.. code-block:: python
   :linenos:

   B1 = G.B0/np.max(connectBuildings) * 2
   p = -G.B0 + B1*connectBuildings
   self.Grow = np.round(inv_logit(p))

Scheduling
----------

The scheduling of the model framework is a simple continuous simulation.
On each time step the model computes for each cell the transitions and
implements them.
The code below shows the definition of the `step` function, call within a while
loop.

On each time step the model re-computes the energy price index (cl:4) and both
transition arrays (RetrofitTransitions cl:6 and GridTransitions cl:7).

The order in which the `step()` function calls the other models functions is
important.
The building variable `ConnectionWish` is computed within the building function
`computeRetrofitsTransitions()`, this variable is used by the grid function
`computeGridTransitions()` and therefor needs to be computed first.

Finally the model computes the expansion of the heat supply grid (cl:8) and
adds the building age of the buildings (cl:10).

This minimal implementation only considers a continuous simulation flow but an
event based simulation could be implemented within a similar framework.

.. code-block:: python
   :linenos:

   def step(self):
       """Simulation step."""
       # Compute energy price index
       energyPrice = G.getEnergyPrice(self.t)
       # Compute transitions
       self.B.computeRetrofitsTransitions(energyPrice)
       self.Gr.computeGridTransitions()
       self.Gr.makeGridGrow()
       # Increase the age of buildings by one
       self.B.Buildings[self.B.Buildings > 0] += 1

Moor neighbourhood
------------------

For the definition of neighbors and neighborhood we make use of the Moor
neighbourhood definition.
Below we present the python code used for the computation of the neighborhood.
The code requires the following input variables for the computation of the moor
neighbourhood:

(1) cell position (i.e. agent for which we compute the neighbourhood);
(2) dimensions of the world grid, defined in global variable `dim`; and
(3) factor `k`, defining the size of the neighbourhood.

.. code-block:: python
   :linenos:

   return [(int(pos[0]+a), int(pos[1]+b)) for a in
           range(k*-1, k+1) for b in range(k*-1, k+1)
           if (a != 0 or b != 0) and
           (pos[0]+a >= 0 and pos[1]+b >= 0) and
           (pos[0]+a < G.dim[0] and pos[1]+b < G.dim[1])
           ]


Amortization time
-----------------

The amortization time of a building retrofit is used as one of the primary
variables for the agents decision to undertake retrofit or not.

The amortization time [Am] is computed as function of: (1) Investment [Inv];
(2) energy price index [Ie]; and (3) energy demand [y], expressed as
building construction year.
The formal expression of amortization time is:

.. math::

    Am = log(Inv / (y * Ie))

The investment is normalized to input variable `maxInitConstructionYear` at t=0
with Ie=1.

Notice that the amortization value is computed as the logarithmic function of
the initial investment cost divided by the energy savings (we use construction
year as proxy) times the price index.
This is just an add-hoc decision in order to flatter the amortization curve.
If we don't perform this control we would assume a linear relationship between
construction year and energy demand. 

Transitions arrays
------------------

**Figure 7** shows the retrofit transition array, black cells represent value 1. 
On time step 30 three buildings will be retrofitted.

.. figure:: FIGURES/world_30_Retrofit.png
   :scale: 60 %
   :alt: building retrofit transitions

   **Figure 7: Buildings transition array**

**Figure 8** shows the transition array for the buildings wanting to connect to
the heat supply grid.
Only buildings that are going to be retrofitted on time step 30 get computed
for the connection wish array.
Out of the three buildings to be retrofitted only two wish to be connected to
the heat supply grid.

.. figure:: FIGURES/world_30_ConnectionWish.png
   :scale: 60 %
   :alt: grid connection wish

   **Figure 8: Buildings wish to connect to the grid**

Design Concepts
===============

In the following section the design concepts of the model are presented and
discussed.
The minimal model implementation aims to use key concepts central to the core
concept of an agent-based model for the simulation of urban dynamics and its
interaction to energy supply networks. 

Basic Principles
----------------

As basic principles of this model implementation we identify:

(1) An interaction between buildings and its underlying infrastructure.
(2) A specific interaction with agent neighbours.
(3) The simulation of a dynamic infrastructure grid.

Interaction
----------------

On this model the interaction between building agents and grid section agents
is limited to the grid expanding based on buildings issuing the wish to get
connected to the grid.

The interaction between buildings and the heat supply grid can be further
develop.

The interaction between building agents is limited to the observation of the
neighbourhood by each building agent.
The retrofit decision of a building has a direct impact on the decision to
retrofit of other neighbours.

Initialization
==============

We need an initial setting for the simulation.
For this minimal model implementation we create a simple synthetic world with
an initial set of buildings and an underlying heat supply grid.

The initial buildings are uniformly distributed in space.
Each building is attributed an initial construction year.
The distribution of construction years is also uniformly distributed among
buildings.
From the generated buildings a random set is selected and attributed a
performed retrofit.
This step is relevant because one of the parameters influencing the decision of
buildings to retrofit or not depends of neighbouring buildings having performed
a retrofit before.

A random sample of buildings is selected to be connected to the heat supply
grid.
Based on this selection a grid is lay out.
For the construction of the heat supply grid we select a random cell and draw a
path to the nearest connected buildings and from them until the next building
until all connected buildings have access to the grid.

The following parameters define the initial synthetic world created for the
simulation. 

(1) `connectionShare`:
    
    Share of buildings connected to the grid. This parameter is used to select
    the sample of buildings connected to the heat supply grid.

(2) `buildingsDensity`:
    
    Building density of the synthetic world. This parameter defines the number
    of buildings in the simulation world.

(3) `maxInitConstructionYear`:
    
    Maximum value for building year. This parameter is needed for attributing
    the buildings with a random construction year. This is the maximum possible
    value that a building can take. This is not the maximum value that
    buildings can have during simulation.

(4) `initialRenovatedShare`:
    
    Share of retrofitted buildings. We need this value for the selection of the
    buildings having a previous retrofit. 

(5) `dim`:
    
    Length and width of random world. The synthetic world can only be of
    rectangular shape, this parameters defines the dimension of the synthetic
    world.

**Figure 4** shows the initial state of the synthetic buildings.
The color scale of the plot represents the initial building construction year.

.. figure:: FIGURES/world_0_Buildings.png
   :scale: 60 %
   :alt: random initial buildings

   **Figure 4: Random initial state of buildings**

**Figure 5** shows the number of performed retrofits per building.
A predefine number of buildings is randomly selected and attributed a
previously performed retrofit. 

.. figure:: FIGURES/world_0_Renovations.png
   :scale: 60 %
   :alt: random initial grid

   **Figure 5: Randomly selected buildings with a performed retrofit**

**Figure 6** shows the underlying heat supply grid for the previously generated
synthetic world.
This array differentiates between four different states.
If there is no grid on a cell the array represents it with a -1, is there is
a grid section on that cell the array will represent this with a zero.
If a building is connected to a section of the grid the array represents it by
a one.
The initial random point is represented by a two, this could be interpreted as
the heat generation source.

This initial random state can be reproduce through the definition of the random
number generator seed defined as a global variable.

The developed ruled for the creation of the heat supply grid need to be further
elaborated in order to generate a more realistic layout of the heat supply
grid.
As an alternative to the generation of a heat supply grid we aim to represent
an existing heat supply grid derived from real input data. 

.. figure:: FIGURES/world_0_Grid.png
   :scale: 60 %
   :alt: random initial grid

   **Figure 6: Cooresponding random grid layout**

Simulation Results
==================

The simulation results presented on this example show the functionality of the
presented model.

**Figure 9** shows the state of the building stock at t=30 (i.e. after 30
simulation iterations).
At this stage almost every building has been retrofitted and has a construction
year close to zero.

There are still some buildings that have not been retrofitted. 
The simulation runs until every building as been retrofitted at least one time.
In this example the simulation runs until t=35.

.. figure:: FIGURES/world_30_Buildings.png
   :scale: 60 %
   :alt: buildings t=30

   **Figure 9: Buildings at t=30**

**Figure 10** shows the number of performed retrofits per building, a building
can have more than a single retrofit on its life time.
At time step 30 almost every building has been retrofit at least one time.
Some building have start retrofitting a second time.

.. figure:: FIGURES/world_30_Renovations.png
   :scale: 60 %
   :alt: renovations t=30

   **Figure 10: Buildings renovations at t=30**

The state of the heat supply grid at time step (t=30) can be seen on **Figure 11**.
As defined on the global parameter `connectionThreshold=0.5` many building want
to connect to the heat distribution grid (every second building on average).

The defined rules for the expansion of the heat supply grid are rather
primitive.
The algorithm will simply connect two points following the shortest path
between them.
In order to create a more realistic growth pattern of the heat supply grid we
need to define more elaborated rules controlling the layout of the heat supply
grid.

.. figure:: FIGURES/world_30_Grid.png
   :scale: 60 %
   :alt: grid at t=30

   **Figure 11: Grid at time step t=30**

The model has a primitive logging function that records certain variables at
each time step.
**Figure 12** shows the development of the following variables over simulation
time:

(1) Energy price index, is the computed price index at each time step. For now
    this variables is not affected by any other external factor.
(2) Performed retrofits, are the total number of retrofits performed at each
    time step. 
(3) Grid expansion, plots the total number of grid sections that had a growth
    behaviour on each time step.
(4) Connection wish, shows the total number of buildings with a wish to get
    connected to the heat supply grid at each time step.
(5) The variable mean retrofits per buildings plots the computed mean of all
    the performed retrofits per building at each simulation step.

.. figure:: FIGURES/log.png
   :scale: 60 %
   :alt: simulation-log

   **Figure 12: Plot of the simulation log at step t=30**

Shortcomings of the implementation
==================================

We identify some shortcomings on this implementation of the model.
The biggest problem with this implementation of the heat supply grid expansion
rules.

**Figure 11** shows the state of the heat supply grid after 30 time steps.
The figure shows a uncontrolled growth pattern of the heat supply grid.
In order to crate a more realistic scenario we need to define more elaborated
rules defining the growth of the infrastructure network.

This model implementations only considers a static building stock, we do not
simulate demolitions of new constructions.
On a second implementation we need to develop rules for the construction of new
buildings and for the demolition of old ones.

On this model we work with a synthetic world.
The use of real data as input for the model requires the development of special
modules recovering, interpreting and processing this data.

The retrofits transition array takes only two attributes as input: amortization
time and retrofitted neighbours.
This rule needs to take more elaborated rules into account.
If we only consider these two attributes for the estimation of the retrofit
transition array we get a very predictable outcome. 
A concentration of retrofits in the older city center, see [Munoz.2015]_.
This scenario is an unlikely scenario, in order to reproduce a more likely
scenario we need to take more attributes into account.

The presented model implementation computes the beta coefficients (β1 and β2)
dynamically (i.e. recomputes the  coefficients at each time step) normalizing
them to the maximal value of the corresponding variable at a given time step. 
This behaviour might not be desire, in order to define the beta coefficients as
global variables we need empirical data for the calibration of the model.
Such data might not be available, in this case we can calibrate the model at an
aggregated level, aligning the results to available aggregated data.

The model framework uses a simple raster for the representation of space and
storage of agent variables.
The biggest advantage of this type of data structure is speed.
For the incorporation of real data into the model we need to either develop a
more elaborated data structure of develop a module for the representation of
real data into this raster structure.

.. raw:: pdf

    PageBreak oneColumn

.. include:: ./bib.rst


