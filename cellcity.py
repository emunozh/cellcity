#!/usr/bin/env python
# -*- coding:utf -*-
"""
Created by Esteban Munoz (emunozh@gmail.com)

Mon 23 May 2016 12:09:26 PM CEST

updates:
    (1) finish first prototype. (EM)
        Thu 02 Jun 2016 04:11:17 PM CEST

"""
import random
from copy import copy
from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker


def energyLog(t):
    return 1 + np.log(t + 1)


class Log():
    """
    class for the logging of variables.
    """

    def __init__(self):
        """
        log class initiator.
        """
        self.x = list()
        self.Ei = list()  # Energy index
        self.retrofits = list()
        self.gridGrow = list()
        self.connectionWish = list()
        self.meanRetrofits = list()

    def plotLog(self, save=False):
        """
        plot log variables.
        """
        fig, ax = plt.subplots(nrows=5, sharex=True, figsize=(10, 5))
        ax[0] = self.plotVar(ax[0], self.Ei, "Energy price index")
        ax[1] = self.plotVar(ax[1], self.retrofits, "Performed retrofits")
        ax[2] = self.plotVar(ax[2], self.gridGrow, "Grid expansion")
        ax[3] = self.plotVar(ax[3], self.connectionWish, "Connection Wish")
        ax[4] = self.plotVar(ax[4], self.meanRetrofits, "Mean retrofits per building")
        ax[4].set_xlabel("Time step (t)")
        plt.tight_layout()
        if save:
            plt.savefig("FIGURES/log.png")
        else:
            plt.show()

    def plotVar(self, ax, var, title):
        """
        Plot single variable.
        """
        stepsize = max(var)/3
        ax.plot(self.x, var)
        start, end = ax.get_ylim()
        ax.yaxis.set_ticks(np.arange(start+stepsize/2, end, stepsize))
        ax.set_title(title)
        ax.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
        return ax


class G():
    """
    class containing global variables.
    """
    # Input simulation parameters
    connectionThreshold = 0.5
    initialEnergyPrice = 0
    getEnergyPrice = energyLog
    B0 = 10                # Transition probability B0 coefficient
    retrofitsTimeSpan = 2  # time span to look for retrofits
    neighbourhood = 3      # k moor neighbourhood parameter
    gridCatchmentArea = 5  # catchment area fro grid to expand
    # Input parameters for the creation of a synthetic world
    connectionShare = 0.1
    buildingsDensity = 0.1
    maxInitConstructionYear = 50
    initialRenovatedShare = 0.1
    # length and width of random world
    dim = [40, 40]
    # Defines a seed for reproducibility.
    # random seed
    seed = 5
    random.seed(seed)
    np.random.seed(seed)


class BuildingSpace():
    """
    class describing the buildings agents.
    """

    def __init__(self, filePathBuildings=False, filePathRenovations=False):
        """
        Buildings class initiator.
        """
        if not filePathBuildings:
            self.generateWorld()
        else:
            self.readWorld()

        if not filePathRenovations:
            self.initRenovations()
        else:
            # TODO implement
            pass

    def readWorld(self):
        """
        read a file describing a predefine world with buildings on it.
        """
        # TODO implement
        pass

    def generateWorld(self):
        """
        Creates a virtual world with dimensions l and w.
        """
        self.Buildings = np.ones(shape=G.dim) * -1
        self.N = int(self.Buildings.size * G.buildingsDensity)
        inx = np.random.choice(
            [i for i in range(self.Buildings.size)],
            self.N, replace=False)
        self.Buildings.flat[inx] = np.random.randint(
            G.maxInitConstructionYear, size=self.N)

    def initRenovations(self):
        """
        Select renovated buildings with a given priority p.
        """
        self.Renovations = np.zeros(shape=G.dim)
        retrofitedN = int(self.N * G.initialRenovatedShare)
        selP = self.Buildings.flatten() + 1
        selP = selP / np.sum(selP)

        inx = np.random.choice(
            [i for i in range(self.Buildings.size)],
            retrofitedN, replace=False, p=selP)
        self.Renovations.flat[inx] = 1

    def computeRetrofitsTransitions(self, energyPrice):
        """
        Compute the retrofit transition probabilities based on:

        (1) Amortization time.
        (2) Number of neighbours that have retrofitted within a predefine time
            span.
        """
        # (1) compute amortization
        amortization = np.zeros(shape=G.dim)
        inx = self.Buildings > 0
        y = self.Buildings[inx]
        amortization[inx] = np.log(G.maxInitConstructionYear / (y*energyPrice))
        # (2) compute Retrofits
        inx1 = [i for i in range(self.Buildings.size) if inx.flat[i]]
        retrofitNeighbours = np.zeros(shape=G.dim)
        inx1_un = list(np.unravel_index(inx1, G.dim))
        retrofitNeighbours.flat[inx1] = self.getRetrofitNeighbours(inx1_un)

        if np.sum(retrofitNeighbours) == 0:
            print("Warning: no retrofits on neighbours!")
            B2_retro = 1
        else:
            B2_retro = retrofitNeighbours[inx] *\
                (G.B0/np.max(retrofitNeighbours[inx]))

        B1 = G.B0/np.max(amortization[inx] * -1)

        p = -G.B0 + B1 * amortization[inx] * -1 + B2_retro
        self.Retrofit = np.zeros(shape=G.dim) * -1
        self.Retrofit[inx] = np.round(inv_logit(p))

        inx2 = self.Retrofit == 1
        self.ConnectionWish = np.zeros(shape=G.dim)
        n = self.ConnectionWish[inx2].size
        self.ConnectionWish[inx2] = [self.connect() for i in range(n)]

        self.Renovations[inx2] += 1
        self.Buildings[inx2] = 0

    def getRetrofitNeighbours(self, inx_un):
        """
        Compute how many neighbours have already retrofitted within a predefine
        time span.
        """
        sumRetrofits = list()
        for i, j in zip(inx_un[0], inx_un[1]):
            moorBuilding = moor((i, j), k=G.neighbourhood)
            sumRetrofits.append(np.sum(
                [1 for k in moorBuilding if
                 self.Renovations[k] >= 1 and
                 self.Buildings[k] <= G.retrofitsTimeSpan]
                                   ))
        return sumRetrofits

    def connect(self):
        """
        Select if a building gets connected to the heat distribution grid based
        on a connection probability threshold defined by global variable
        connectionThreshold.
        """
        q = random.random()
        if q < G.connectionThreshold:
            return 1
        else:
            return 0


class GridSpace():
    """
    class describing the grid section agents.
    """

    def __init__(self, B):
        """
        Grid class initiator.
        """
        self.B = B
        self.Grid = np.ones(shape=G.dim) * -1
        self.constructGrid()

    def constructGrid(self):
        """
        Construct a grid giving an initial state of buildings.
        """
        # connected buildings
        connectedN = int(self.B.N * G.connectionShare)
        b = np.reshape(self.B.Buildings >= 0, -1)
        inx = [i for i in range(len(b)) if b[i]]
        inx = np.random.choice(inx, connectedN, replace=False)
        inx_un = list(np.unravel_index([inx], G.dim))
        self.Grid.flat[inx] = 1
        # random initial position of grid (energy production)
        initPos = np.random.randint(self.Grid.size)
        if self.Grid.flat[initPos] < 1:
            self.Grid.flat[initPos] = 2
        initPos_un = np.unravel_index([initPos], G.dim)
        # connect grid to closest point
        connectionNodes = len(inx_un[0][0])
        connected = 0
        while connected < connectionNodes:
            d = self.closestPoint(initPos_un, inx_un)
            self.drawPath(initPos_un, [inx_un[0][0][d], inx_un[1][0][d]])
            initPos_un = [inx_un[0][0][d], inx_un[1][0][d]]
            inx_un = np.array(inx_un).copy()
            inx_un[0][0][d] = G.dim[0] * 4
            inx_un[1][0][d] = G.dim[1] * 4
            connected += 1

    def closestPoint(self, p, V):
        """
        Returns the closest point to p of vector V.
        """
        d = [sqrt((p[0]-V[0][0][i])**2 +
                  (p[1]-V[1][0][i])**2) for i in range(len(V[0][0]))]
        return int([e for e, i in enumerate(d) if i == min(d)][0])

    def drawPath(self, o, i):
        """
        Draw a path between initial point o and final point i.
        """
        i = (int(i[0]), int(i[1]))
        o = (int(o[0]), int(o[1]))
        if i not in moor(o, k=1):
            y1, x1 = o
            y2, x2 = i
            dx = x2 - x1
            dy = y2 - y1
            signx = 1
            signy = 1
            if dx == 0:
                dx = 1
            elif dx < 0:
                signx = -1
            if dy == 0:
                dy = 1
            elif dy < 0:
                signy = -1
            if abs(dx) >= abs(dy):
                for x in range(x1, x2+signx, signx):
                    y = int(y1 + dy * (x - x1) / dx)
                    if self.Grid[y][x] < 0:
                        self.Grid[y][x] = 0
            else:
                for y in range(y1, y2+signy, signy):
                    x = int(x1 + dx * (y - y1) / dy)
                    if self.Grid[y][x] < 0:
                        self.Grid[y][x] = 0

    def computeGridTransitions(self):
        """
        Compute the grid grow transitions based on:

        (1) Buildings that want to retrofit within a predefined catchment area
        """
        connectBuildings = np.zeros(shape=G.dim)
        inx = self.Grid >= 0
        inx = [i for i in range(self.Grid.size) if inx.flat[i]]
        inx_un = list(np.unravel_index(inx, G.dim))
        connectBuildings.flat[inx] = self.getBuildingsConnectionPotential(
            inx_un)

        self.Grow = np.zeros(shape=G.dim)
        if np.sum(connectBuildings) == 0:
            print("Warning: no buildings to connect!")
        else:
            B1 = connectBuildings.flat[inx] * (
                G.B0/np.max(connectBuildings.flat[inx])) * 2
            p = -G.B0 + B1
            self.Grow.flat[inx] = np.round(inv_logit(p))

    def getBuildingsConnectionPotential(self, inx_un):
        """
        Compute how many buildings within the catchment area of a grid node
        want will retrofit and want to connect to the grid.
        """
        sumConnections = list()
        for i, j in zip(inx_un[0], inx_un[1]):
            moorGrid = moor((i, j), k=G.gridCatchmentArea)
            a = np.sum(
                [1 for k in moorGrid if self.B.Retrofit[k] == 1])
            b = np.sum(
                [1 for k in moorGrid if self.B.ConnectionWish[k] == 1])
            sumConnections.append(a+b)
        return sumConnections

    def makeGridGrow(self):
        """
        Increase of reduce the grid network based on Grow transition array.
        """
        inx_p = self.B.ConnectionWish > 0
        inx = [i for i in range(self.B.ConnectionWish.size) if inx_p.flat[i]]
        self.Grid.flat[inx] = 1
        inx_un = list(np.unravel_index(inx, G.dim))
        for i, j in zip(inx_un[0], inx_un[1]):
            moorGrid = moor((i, j), k=G.gridCatchmentArea)
            V = np.array([n for n in moorGrid if self.Grow[n] == 1])
            if len(V) > 1:
                d = [np.sqrt((a + i) ^ 2 + (b + j) ^ 2) for a, b in V]
                self.drawPath((i, j), V[d == min(d)][0])


class World():
    """
    class containing the simulation parameters and simulation framework.
    """

    def __init__(self, k=1, maxSteps=False):
        """
        world class initiator.
        """
        if isinstance(maxSteps, bool):
            self.maxSteps = float('Inf')
        else:
            self.maxSteps = maxSteps
        self.t = 0
        self.energyPrice = 0

        self.B = BuildingSpace()
        self.Gr = GridSpace(self.B)

        self.log = Log()

    def allRetrofitted(self):
        """
        Computes if all buildings got retrofitted at least ones.
        """
        b = self.B.Renovations[self.B.Renovations > 0].size
        a = self.B.Buildings[self.B.Buildings > 0].size
        if a == 0 or b >= self.B.N:
            print("All buildings are retrofitted")
            return True
        else:
            return False

    def run(self):
        """
        Start simulation.
        """
        while not self.allRetrofitted() and self.t < self.maxSteps:
            self.t += 1
            print("Simulating time step: ", self.t, " ...")
            self.step()

    def step(self):
        """
        Simulation step.
        """
        energyPrice = G.getEnergyPrice(self.t)
        self.log.x.append(self.t)

        self.B.computeRetrofitsTransitions(energyPrice)
        self.Gr.computeGridTransitions()
        if np.sum(self.Gr.Grow) >= 1 and np.sum(self.B.ConnectionWish) >= 1:
            self.Gr.makeGridGrow()
        else:
            print("Warning: no change in the heat supply grid")
        # Increase the age of buildings by one
        self.B.Buildings[self.B.Buildings > 0] += 1
        # store variables in log
        self.log.Ei.append(energyPrice)
        self.log.retrofits.append(np.sum(self.B.Retrofit))
        self.log.connectionWish.append(np.sum(self.B.ConnectionWish))
        self.log.gridGrow.append(np.sum(self.Gr.Grow))
        self.log.meanRetrofits.append(np.mean(self.B.Renovations))

    def plotWorld(self, save=False):
        """
        Plot world state.
        """
        # Plot Buildings
        plotSingle(self.B.Buildings, save, 'Buildings', self.t,
                   ylabel='Age (years since last renovation)')
        # Plot Grid
        plotSingle(self.Gr.Grid, save, 'Grid', self.t, cmap='YlOrRd',
                   ylabel='Grid connection')
        # Plot Renovations
        plotSingle(self.B.Renovations, save, 'Renovations', self.t,
                   cmap='Blues', minVal=1, ylabel='Number of renovations')
        # Computed transition arrays
        # (1) Grow
        if 'Grow' in dir(self.B):
            plotSingle(self.Gr.Grow, save, 'Grow', self.t, cmap='Greys',
                       minVal=-1, printlabel=False)
        # (2) ConnectionWish
        if 'ConnectionWish' in dir(self.B):
            plotSingle(self.B.ConnectionWish, save, 'ConnectionWish', self.t,
                       cmap='Greys', minVal=-1, printlabel=False)
        # (3) Retrofit
        if 'Retrofit' in dir(self.B):
            plotSingle(self.B.Retrofit, save, 'Retrofit', self.t, cmap='Greys',
                       minVal=-1, printlabel=False)


def plotSingle(plotW, save, name, t, cmap='brg', ylabel='', minVal=0,
               printlabel=True):
    """
    Make a single plot for arrays: (1) Buildings; (2) Grid; (3) Retrofits.
    """
    plotB = copy(plotW)
    plotB[plotB < minVal] = np.nan

    fig, ax = plt.subplots(figsize=(5, 5))

    imgplotB = ax.imshow(plotB, interpolation='nearest', cmap=cmap)
    ax.set_title('{} t={}'.format(name, t))
    ax.set(aspect=1, adjustable='box-forced')
    ax.axis('off')
    if printlabel:
        cbar = fig.colorbar(imgplotB, ax=ax)
        cbar.ax.get_yaxis().labelpad = 15
        cbar.ax.set_ylabel(ylabel, rotation=270)

    if save:
        plt.savefig("FIGURES/world_{}_{}.png".format(t, name),
                    bbox_inches='tight')
    else:
        plt.show()


def moor(pos, k=1):
    """
    Returns the corresponding moor neighbourhood with size k for position pos.
    The world limits are define by variables i and j.

    **Example 1:** Neighbourhood of cell on the upper edge of the world.

    >>> n = moor([0, 2], k=4)
    >>> o = [
    ... (0, 0), (0, 1),         (0, 3), (0, 4),
    ... (1, 0), (1, 1), (1, 2), (1, 3), (1, 4),
    ... (2, 0), (2, 1), (2, 2), (2, 3), (2, 4),
    ... (3, 0), (3, 1), (3, 2), (3, 3), (3, 4),
    ... (4, 0), (4, 1), (4, 2), (4, 3), (4, 4)]
    >>> assert(n == o)

    **Example 2:** Neighbourhood of cell on the upper left corner of the
    world.

    >>> n = moor([0, 0], k=4)
    >>> o = [
    ...         (0, 1), (0, 2), (0, 3), (0, 4),
    ... (1, 0), (1, 1), (1, 2), (1, 3), (1, 4),
    ... (2, 0), (2, 1), (2, 2), (2, 3), (2, 4),
    ... (3, 0), (3, 1), (3, 2), (3, 3), (3, 4),
    ... (4, 0), (4, 1), (4, 2), (4, 3), (4, 4)]
    >>> assert(n == o)

    **Example 3:** Neighbourhood of the cell on the lower right corner of
    the world.

    >>> n = moor([4, 4], k=4)
    >>> o = [
    ... (0, 0), (0, 1), (0, 2), (0, 3), (0, 4),
    ... (1, 0), (1, 1), (1, 2), (1, 3), (1, 4),
    ... (2, 0), (2, 1), (2, 2), (2, 3), (2, 4),
    ... (3, 0), (3, 1), (3, 2), (3, 3), (3, 4),
    ... (4, 0), (4, 1), (4, 2), (4, 3)        ]
    >>> assert(n == o)

    **Example 4: Neighbourhood of the cell on the middle of the world.

    >>> n = moor([2, 2], k=4)
    >>> o = [
    ... (0, 0), (0, 1), (0, 2), (0, 3), (0, 4),
    ... (1, 0), (1, 1), (1, 2), (1, 3), (1, 4),
    ... (2, 0), (2, 1),         (2, 3), (2, 4),
    ... (3, 0), (3, 1), (3, 2), (3, 3), (3, 4),
    ... (4, 0), (4, 1), (4, 2), (4, 3), (4, 4)]
    >>> assert(n == o)
    """
    return [(int(pos[0]+a), int(pos[1]+b)) for a in
            range(k*-1, k+1) for b in range(k*-1, k+1)
            if (a != 0 or b != 0) and
            (pos[0]+a >= 0 and pos[1]+b >= 0) and
            (pos[0]+a < G.dim[0] and pos[1]+b < G.dim[1])
            ]


def inv_logit(p):
    """
    Inverse logit function.
    """
    return np.exp(p) / (1 + np.exp(p))


def main():
    w = World(maxSteps=30)
    w.plotWorld(save=True)
    w.run()
    w.plotWorld(save=True)
    w.log.plotLog(save=True)

if __name__ == "__main__":
    main()
    # import doctest
    # doctest.testmod()
