References
==========

.. [Grimm.2010] Grimm, V.; Berger, U.; DeAngelis, D. L.; Polhill, J. G.; Giske, J. & Railsback, S. F. The ODD protocol: A review and first update Ecological Modelling , 2010, 221, 2760 - 2768 

.. [Blalock.1979] Blalock, H. M. Social statistics (Rev. 2d ed ed.). New York: McGraw-Hill, 1979

.. [Polhill.2008] Polhill, J. G.; Parker, D.; Brown, D. & Grimm, V.  Using the ODD Protocol for Describing Three Agent-Based Social Simulation Models of Land-Use Change Journal of Artificial Societies and Social Simulation, 2008, 11, 3 

.. [Polhill.2010] Polhill, J. G. ODD Updated Journal of Artificial Societies and Social Simulation, 2010, 13, 9

.. [Munoz.2015] Muñoz H., M. E.; Vidyattama, Y. & Tanton, R. The Influence of an Ageing Population and an Efficient Building Stock on Heat Consumption Patterns 14th International Conference of the International Building Performance Simulation Association (IBPSA), 2015
